```
#!html
アプリケーション(VMBackupper)が起動したときにNewLinuxServerという名前の仮想マシンを起動し、その仮想マシンが終了したら、自動的に仮想マシンのファイルを7zで圧縮します。
バックアップ先はC:\Users\ユーザー名\AppData\Roaming\junT58\VMB\Backupに保存されます。解凍には7-zip等のソフトが必要です。
ライセンス: GNU LGPLv3(http://www.gnu.org/licenses/lgpl.html)
仕様ライブラリ:SevenZipSharp.dll(LGPL v3)、7z.dll(LGPL)
必要ソフト
.Net framework 4.5以上
Oracle VM VirtualBox
  インストール場所はC:\Program Files\Oracle\VirtualBox\であることが必須
Copyright © 2014 junT58
```