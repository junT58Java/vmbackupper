﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using SevenZip;
using System.IO;

namespace VMBackupper
{
    public partial class Form1 : Form
    {
        private int count = 0;
        private int exitting = 0;
        private int button = 0;
        private String lab1 = "";
        private String lab2 = "";
        private String lab3 = "";
        private int persent = 0;

        public Form1()
        {
            lab1 = "□ Starting VM Machine";
            lab2 = "□ Waiting Closed VM Machine";
            lab3 = "□ Backupping";
            InitializeComponent();
            threads();
            Button1.Enabled = false;
        }

        public void threads()
        {
            Thread threadA = new Thread(new ThreadStart(runVM));
            threadA.Start();
        }

        public void backup()
        {
            String user = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            String folderpath = user + "\\VirtualBox VMs\\NewLinuxServer";
            SevenZipCompressor szc = new SevenZipCompressor();
            szc.CompressionMethod = CompressionMethod.Lzma;
            szc.CompressionMode = CompressionMode.Create;
            szc.CompressionLevel = CompressionLevel.Normal;
            szc.Compressing += new EventHandler<ProgressEventArgs>(progress);
            string appPath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            DateTime dt = DateTime.Now;
            int year = dt.Year;
            int month = dt.Month;
            int day = dt.Day;
            int hour = dt.Hour;
            int minute = dt.Minute;
            int second = dt.Second;
            String filename = year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + "-backup.7z";
            String fullpath = user + "\\AppData\\Roaming\\junT58\\VMB\\Backup\\" + filename;
            if (!Directory.Exists(user + "\\AppData\\Roaming\\junT58\\VMB\\Backup"))
            {
                Directory.CreateDirectory(user + "\\AppData\\Roaming\\junT58\\VMB\\Backup");
            
            }
            string[] files = System.IO.Directory.GetFiles(folderpath, "*", System.IO.SearchOption.AllDirectories);
            SevenZipCompressor.SetLibraryPath(appPath + "\\7z.dll");
            szc.CompressFiles(fullpath, files);
            lab3 = "☑ Backupping";
            button = 1;
        }
        public void progress(object obj, ProgressEventArgs e)
        {
            persent = e.PercentDone;
        }

        public void runVM()
        {
            System.Diagnostics.Process p = System.Diagnostics.Process.Start("C:\\Program Files\\Oracle\\VirtualBox\\VirtualBox.exe", @"-startvm NewLinuxServer");
            lab1 = "☑ Starting VM Machine";
            p.WaitForExit();
            lab2 = "☑ Waiting Closed VM Machine";
            backup();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            exitting = 1;
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            count++;
            if (count == 30)
            {
                exitting = 1;
                Application.Exit();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (exitting == 0)
            {
                e.Cancel = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Label1.Text != lab1)
            {
                Label1.Text = lab1;
            }
            if (Label2.Text != lab2)
            {
                Label2.Text = lab2;
            }
            if (Label3.Text != lab3)
            {
                Label3.Text = lab3;
            }
            if (progressBar1.Value != persent)
            {
                progressBar1.Maximum = 100;
                progressBar1.Value = persent;
            }
            if (Button1.Enabled == false && button == 1)
            {
                Button1.Enabled = true;
                timer1.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
